#!/usr/bin/env bash

docker stop airflow_db
docker network rm airflow_net
docker network create airflow_net

docker pull postgres:12.3-alpine

docker run --net=airflow_net -d --rm --name airflow_db -p 5432:5432 -e POSTGRES_PASSWORD=die_pass postgres:12.3-alpine

INTERACTIVE=''
if [ "$1" == 'INTERACTIVE' ]; then
  INTERACTIVE='-it --entrypoint bash'
fi

docker run --net=airflow_net --rm --name airflow_app -p 8080:8080 \
  -e AIRFLOW_DB_HOST=airflow_db \
  -e AIRFLOW_DB_ADMIN_USR=postgres \
  -e AIRFLOW_DB_ADMIN_USR_PASSWD=die_pass \
  -e AIRFLOW_DB_NAME=wirbel_db \
  -e AIRFLOW_DB_USR=wirbel_usr \
  -e AIRFLOW_DB_USR_PASSWD=das_pass \
  $INTERACTIVE local/wirbel:0.0.1
