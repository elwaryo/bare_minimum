#!/usr/bin/env bash

cd "$(dirname "$0")" || exit 1

./setup_airflow_db.sh

source airflow.environment

airflow initdb

echo "Running web server"

airflow scheduler --daemon &
airflow webserver -p 8080 --daemon
