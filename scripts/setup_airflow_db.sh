#!/usr/bin/env bash

cd "$(dirname "$0")" || exit 1

ERROR=0
if [[ -z "$AIRFLOW_DB_HOST" ]]; then
  echo 'AIRFLOW_DB_HOST not set'
  ERROR=1
fi

if [[ -z "$AIRFLOW_DB_ADMIN_USR" ]]; then
  echo 'AIRFLOW_DB_ADMIN_USR not set'
  ERROR=1
fi

if [[ -z "$AIRFLOW_DB_ADMIN_USR_PASSWD" ]]; then
  echo 'AIRFLOW_DB_ADMIN_USR_PASSWD not set'
  ERROR=1
fi

if [[ -z "$AIRFLOW_DB_NAME" ]]; then
  echo 'AIRFLOW_DB_NAME not set'
  ERROR=1
fi

if [[ -z "$AIRFLOW_DB_USR" ]]; then
  echo 'AIRFLOW_DB_USR not set'
  ERROR=1
fi

if [[ -z "$AIRFLOW_DB_USR_PASSWD" ]]; then
  echo 'AIRFLOW_DB_USR_PASSWD not set'
  ERROR=1
fi

if [ $ERROR == 1 ]; then
  exit 1
fi

sleep 15 # wait for the DB to be available
sed -e "s/__DB__/$AIRFLOW_DB_NAME/g; s/__USR__/$AIRFLOW_DB_USR/g; s/__PASSWORD__/$AIRFLOW_DB_USR_PASSWD/g" setup_airflow_db.sql | PGPASSWORD="$AIRFLOW_DB_ADMIN_USR_PASSWD" psql -h "$AIRFLOW_DB_HOST" -U "$AIRFLOW_DB_ADMIN_USR" -d "$AIRFLOW_DB_ADMIN_USR"
